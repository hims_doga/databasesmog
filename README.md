This code base is for running Mixture of Gaussians on the dataset for clustering.

There are two branches - Master(For the case when data is assumed to fit the memory completely) and MemNoFit(For the case when data does not fit the memory completely.)

The files to be run are:

1. MogM(for materialize)
2. MogFl(For factorized learning).