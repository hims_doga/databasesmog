package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

import org.apache.commons.math3.distribution.MultivariateNormalDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

public class MogM {
	static int clusters=3;
	static int dimr=20;
	static int dims=10;
	static int dimm=30;
	static int dR=20;
	static int dS=10;
	static int NS =2000;
	static int dM=30;
	
	static void Estep(int check,double []Nk,HashMap<Integer,GammaN> intmTable,HashMap<Integer,CoordinatesR> mtable,double[][] mu,ArrayList<RealMatrix> sigma,double[] pik ){
		GammaN res = new GammaN();
		res.f = new double[clusters];
		for (int i=1;i<=mtable.size();i++){	
			double sum=0;
			res = new GammaN();
			res.f = new double[clusters];
			for(int j=0;j<clusters;j++){
				double[] merged=mtable.get(i).fl.get(j);
				double meanz[] = new double[dM];
				Arrays.fill(meanz, 0);
				MultivariateNormalDistribution No = new MultivariateNormalDistribution(meanz,sigma.get(j).getData());
				double result=No.density(merged);
				res.f[j]=result;
				sum=sum+pik[j]*result;
			}
			for(int j=0;j<clusters;j++){
				try{
				res.f[j]=(pik[j]*res.f[j])/sum;
				Nk[j]=Nk[j]+res.f[j];
				} catch(Exception e){
					e.printStackTrace();
				}
			}
			intmTable.put(i, res);
			res=null;
		}	
		for(int j=0;j<clusters;j++){
			pik[j]=Nk[j]/NS;
			if (pik[j]==1.0)
				check=1;
		}
	}
	
	static void MstepMu(double []Nk,HashMap<Integer,GammaN> intmTable,HashMap<Integer,CoordinatesR> mtable,double[][] mu,ArrayList<RealMatrix> isigma){
		double[][] sum = new double[clusters][dM];
		for (int i=1;i<=mtable.size();i++){
			for(int j=0;j<clusters;j++){
				for(int k=0;k<dM;k++){
					  sum[j][k]=sum[j][k]+(intmTable.get(i).f[j])*(mtable.get(i).f.get(k));
				}
			}
		}
		for (int i=0;i<clusters;i++){
			for(int k=0;k<dM;k++){
			mu[i][k]=sum[i][k]/Nk[i];
			}
		}	
	}
	
	static void MstepSig(double []Nk,HashMap<Integer,GammaN> intmTable,HashMap<Integer,CoordinatesR> mtable,double[][] mu,ArrayList<RealMatrix> sigma ){
		sigma = new ArrayList<RealMatrix>();
		for (int i=0;i<clusters;i++){		
			double [][] sum = new double[dM][dM];
			RealMatrix sumR = MatrixUtils.createRealMatrix(sum);
			for (int j=1;j<=mtable.size();j++){
				double [][] dp = new double[dM][1];
				for(int k=0;k<dM;k++){
					 dp[k][0]=mtable.get(j).fl.get(i)[k];
				}
				RealMatrix dataPoint = MatrixUtils.createRealMatrix(dp);
				sumR=sumR.add((dataPoint.multiply(dataPoint.transpose())).scalarMultiply(intmTable.get(j).f[i]));
			}
			sigma.add(sumR.scalarMultiply(1/Nk[i]));
		}
	}
	
	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		HashMap<Integer,CoordinatesR> mtable = new HashMap<Integer,CoordinatesR>();
		HashMap<Integer,GammaN> intmTable = new HashMap<Integer,GammaN>();
		try {
			Class.forName("org.postgresql.Driver");
		} catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
		String database = "Himanshu";
		String username="postgres";
		String password="strive";
		Statement s = null;
		Connection con = null;
		try {
			con = DriverManager.getConnection("jdbc:postgresql:" + database,
												username, password);
			s = con.createStatement();
		} catch(SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		int tries=0;
		//Choose number of iterations here.	
		double[][] mu = new double[clusters][dimm];
		double [] pik = new double[clusters];
		ArrayList<RealMatrix> sigma = new ArrayList<RealMatrix>();
		Random rand = new Random();
		for (int i=0;i<clusters;i++){
			for (int j=0;j<(dimr+dims);j++)
              mu[i][j]=rand.nextDouble();
		}
		String sqln = "drop table mogm;";
		String sql = "create table mogm as select sid,";
			int rcount=1;
		 for (int i=1;i<=dR+dS;i++){
			 if(i<=dR)
		   sql=sql+"x"+i; 
			 else{
				 sql=sql+"y"+rcount;
			     rcount=rcount+1;
			 } 
			 if(i!=dR+dS)
	         sql = sql+",";
	     }
		sql = sql+" from rghr,sghr where rid=fk order by sid;";
		ResultSet rs = null;
		String rsql = "SELECT sid,";
		for(int j=0;j<clusters;j++){
			int count=1;
		 for (int i=1;i<=dR+dS;i++){
			 if(i<=dR){
				 double var =mu[j][i-1];
				 String ttemp1="";
				 if(mu[j][i-1]<0){
					  var = mu[j][i-1]*-1.0;
				      ttemp1=ttemp1+"+"+var;
				 }else if(mu[j][i-1]>=0)
					 ttemp1=ttemp1+"-"+var;
		             rsql=rsql+"x"+i+ttemp1;   
			 }
			 else{
				 double var =mu[j][i-1];
				 String ttemp1="";
				 if(mu[j][i-1]<0){
					  var = mu[j][i-1]*-1.0;
				      ttemp1=ttemp1+"+"+var;
				 }else if(mu[j][i-1]>=0)
					 ttemp1=ttemp1+"-"+var;
					   rsql=rsql+"y"+count+ttemp1; 
				 count=count+1;
			 }
	       if (j!= clusters-1 || i!=dR+dS)
	         rsql = rsql+",";
	     }
		} 
		rsql = rsql+" FROM mogm order by sid";
		try {
			s.executeUpdate(sqln);
			s.executeUpdate(sql);
		} catch(SQLException e) {
			e.printStackTrace();
		}
		int mrid=0;
		try {
			rs = s.executeQuery(rsql);
			while(rs.next()) {
				CoordinatesR c= new CoordinatesR();
				c.fl = new ArrayList<double[]>();
				c.f = new ArrayList<Double>();
				double[] dimInClus = new double[dM];
				int clCount=0;
				int ck=1;
				for (int i=2;i<=((clusters*dimm)+1);i++){
					dimInClus[clCount]=Double.parseDouble(rs.getString(i));
					if(ck==1)
					c.f.add(dimInClus[clCount]+mu[0][clCount]);
					clCount=clCount+1;
					if(clCount == dimm){
					ck=0;
				    c.fl.add(dimInClus);
				    //check of this makes it null;
				    dimInClus=null;
				    dimInClus = new double[dM];
				    clCount=0;
					}	
				}
				mtable.put(Integer.parseInt(rs.getString(1)),c);
				if(mrid<Integer.parseInt(rs.getString(1)))
					mrid=Integer.parseInt(rs.getString(1));
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
			while(tries<3){
				if(tries == 0)
				initSigma(mtable,sigma,pik);
				double[] Nk = new double[clusters];
				//E-step
				int check=0;
				Estep(check,Nk,intmTable,mtable,mu,sigma,pik );
				if (check == 1){
				    System.out.println("converged at iteration "+tries);
					tries=100;
				}
				//M-step
	            MstepMu(Nk,intmTable,mtable,mu,sigma);
		
				rs = null;
				int hint=0;
				rsql = "SELECT sid,";
				for(int j=0;j<clusters;j++){
					int count=1;
				 for (int i=1;i<=dR+dS;i++){
					 if(i<=dR){
						 double var =mu[j][i-1];
						 String ttemp1="";
						 if(mu[j][i-1]<0){
							  var = mu[j][i-1]*-1.0;
						      ttemp1=ttemp1+"+"+var;
						 }else if(mu[j][i-1]>=0)
							 ttemp1=ttemp1+"-"+var;
				      rsql=rsql+"x"+i+ttemp1;  
					 }
					 else{
						 double var =mu[j][i-1];
						 String ttemp1="";
						 if(mu[j][i-1]<0){
							  var = mu[j][i-1]*-1.0;
						      ttemp1=ttemp1+"+"+var;
						 }else if(mu[j][i-1]>=0)
							 ttemp1=ttemp1+"-"+var;
							   rsql=rsql+"y"+count+ttemp1; 
						 count=count+1;
					 }
					 if (Double.isNaN(mu[j][i-1]))
						  hint=1;
			       if (j!= clusters-1 || i!=dR+dS)
			         rsql = rsql+",";
			     }
				} 
				rsql = rsql+" FROM mogm order by sid";
				
				if(hint ==1){
					System.out.println("broke at"+tries);
					break;
				}
				mrid=0;
				try {
					//System.out.println(rsql);
					rs = s.executeQuery(rsql);
					while(rs.next()) {
						CoordinatesR c= new CoordinatesR();
						c.fl = new ArrayList<double[]>();
						c.f = new ArrayList<Double>();
						double[] dimInClus = new double[dM];
						int clCount=0;
						int ck=1;
						for (int i=2;i<=((clusters*dimm)+1);i++){
							dimInClus[clCount]=Double.parseDouble(rs.getString(i));
							if(ck==1)
							c.f.add(dimInClus[clCount]+mu[0][clCount]);
							clCount=clCount+1;
							if(clCount == dM){
							ck=0;
						    c.fl.add(dimInClus);
						    //check of this makes it null;
						    dimInClus=null;
						    dimInClus = new double[dM];
						    clCount=0;
							}	
						}
						mtable.put(Integer.parseInt(rs.getString(1)),c);
						if(mrid<Integer.parseInt(rs.getString(1)))
							mrid=Integer.parseInt(rs.getString(1));
					}
				} catch(SQLException e) {
					e.printStackTrace();
				}
			  MstepSig(Nk,intmTable,mtable,mu,sigma);
			  System.out.println(tries);
              tries=tries+1;		         
	}	
	    long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println(totalTime);
	}
	// common data processing methods
	static void initSigma(HashMap<Integer,CoordinatesR> mtable,ArrayList<RealMatrix> sigma,double[] pik){
		for (int i=0;i<clusters ;i++){
	    NormalDistribution obj= new NormalDistribution(0,1);
	    double [][] sam = new double[dM][dM];
	    for (int j=0;j<(dM);j++)
	    sam[j] =obj.sample(dM);
	    RealMatrix A = MatrixUtils.createRealMatrix(sam);
	    A=A.add(A.transpose()).scalarMultiply(.50);
		RealMatrix trial = MatrixUtils.createRealIdentityMatrix(dM);
	    trial=trial.scalarMultiply(dM);
	    A=A.add(trial);
	    sigma.add(A);
	    pik[i]=1.0/(clusters);
		  }
	}
}