package main;

import java.util.Arrays;

import org.apache.commons.math3.distribution.MultivariateNormalDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.stat.correlation.Covariance;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.Variance;

public class MogRun {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Create a real matrix with two rows and three columns, using a factory
		// method that selects the implementation class for us.
		double[][] matrixData = { {1d,2d,3d,4d,5d,1d,2d,3d,4d,5d}, {1d,2d,5d,3d,6d,7d,2d,3d,4d,5d},{1d,2d,3d,4d,5d,2d,5d,3d,6d,7d}, {2d,5d,3d,6d,7d,2d,5d,3d,6d,7d},{1d,2d,3d,4d,5d,2d,5d,3d,6d,7d}, {2d,5d,3d,6d,2d,5d,3d,6d,7d,7d},{1d,2d,3d,2d,5d,3d,6d,7d,4d,5d}, {2d,5d,2d,5d,3d,6d,7d,3d,6d,7d},{1d,2d,3d,2d,5d,3d,6d,7d,4d,5d}, {2d,5d,3d,2d,5d,3d,6d,7d,6d,7d}};
		double[][] mat= new double [2][2];
		double[][] mat1= new double [1][2];
		mat[0][0]=1;
		mat[0][1]=2;
		mat[1][0]=3;
		mat[1][1]=4;
		mat1[0][0]=5;
		mat1[0][1]=6;
		RealMatrix m = MatrixUtils.createRealMatrix(matrixData);
		RealMatrix m1 = MatrixUtils.createRealMatrix(mat1);
		//System.out.println(MatrixUtils.createColumnRealMatrix(m.getColumn(0)).multiply(m1));

		// One more with three rows, two columns, this time instantiating the
		// RealMatrix implementation class directly.
		double[][] matrixData2 = { {1d,2d,3d,4d,9d,1d,2d,3d,4d,5d}, {1d,2d,5d,3d,6d,7d,2d,3d,4d,5d},{1d,2d,3d,4d,5d,2d,5d,3d,6d,7d}, {2d,5d,3d,6d,7d,2d,5d,3d,6d,7d},{1d,2d,3d,4d,5d,2d,5d,3d,6d,7d}, {2d,5d,3d,6d,2d,5d,3d,6d,7d,7d},{1d,2d,3d,2d,5d,3d,6d,7d,4d,5d}, {2d,5d,2d,5d,3d,6d,7d,3d,6d,7d},{1d,2d,3d,2d,5d,3d,6d,7d,4d,5d}, {2d,5d,3d,2d,5d,3d,6d,7d,6d,7d}};
		RealMatrix n = new Array2DRowRealMatrix(matrixData2);
		long startTime = System.currentTimeMillis();
		RealMatrix andN =MatrixUtils.inverse(n);
		//System.out.println("inverted");
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println(totalTime);
		startTime = System.currentTimeMillis();
		/*DecompositionSolver solver = new LUDecomposition(n).getSolver();
		double[] constants = new double[10];
		Arrays.fill(constants, 1.0);
		RealVector consta =new ArrayRealVector(constants);
		RealVector solution = solver.solve(consta);
		endTime   = System.currentTimeMillis();
		totalTime = endTime - startTime;*/
		NormalDistribution obj1= new NormalDistribution(0,1);
		double ele =Math.abs(obj1.sample(1)[0]);
		System.out.println(totalTime);
		//LUDecomposition a = new LUDecomposition(m);
		//System.out.println(a.getDeterminant());
		//double[] mean = new double [10];
		Variance v = new Variance();
        double var =v.evaluate(n.getColumn(2));
        var=Math.pow(Math.abs(var), .5);
		Mean nm = new Mean();
	    double mu =nm.evaluate(n.getColumn(3));
	    NormalDistribution obj= new NormalDistribution(mu,var);
	    double [][] sam = new double[10][10];
	    for (int i=0;i<10;i++)
	    sam[i] =obj.sample(10);
	    RealMatrix A = MatrixUtils.createRealMatrix(sam);
	    A=A.add(A.transpose()).scalarMultiply(.50);
		RealMatrix trial = MatrixUtils.createRealIdentityMatrix(10);
	    trial=trial.scalarMultiply(10);
	    A=A.add(trial);
        
		double[] mean = {1d,2d,3d,4d,5d,6d,7d,9d,8d,4d};
		double[] point = {2d,4d,3d,6d,2d,7d,4d,8d,5d,7d};
		MultivariateNormalDistribution b = new MultivariateNormalDistribution(mean,	A.getData());
		double result=b.density(point);
		// Note: The constructor copies  the input double[][] array in both cases.

		// Now multiply m by n
		double new1[][]=new double[1][2];
		new1[0][0]=1;
		new1[0][1]=2;
		double new2[][]=new double[2][1];
		new2[0][0]=3;
		new2[1][0]=4;
		double[] nd = new double[3];
		double [][] cm = new double[1][3]; 
		cm[0]= nd;
		RealMatrix new1r = MatrixUtils.createRealMatrix(cm);
		RealMatrix new2r = MatrixUtils.createRealMatrix(new2);
		RealMatrix p = new1r.multiply(new2r);
		double[][] a =p.getData();
		//System.out.println(p.getRowDimension());    // 2
		//System.out.println(p.getColumnDimension()); // 2
        System.out.println(result);

	}

}
