package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

import org.apache.commons.math3.distribution.MultivariateNormalDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.lang3.ArrayUtils;


public class MogFL {
	static int clusters=3;
	static int dimr=20;
	static int dims=10;
	static int dR=20;
	static int dS=10;
	static int NS =2000;
	
	
	static void Estep(int check,double []Nk,HashMap<Integer,GammaN> intmTable,HashMap<Integer,CoordinatesR> rtable,double[][] mu,HashMap<Integer,CoordinatesS> stable,ArrayList<RealMatrix> isigma,double[] pik ){
		GammaN res = new GammaN();
		res.f = new double[clusters];
		for (int i=1;i<=stable.size();i++){	
			//check this
			double sum=0;
			res = new GammaN();
			res.f = new double[clusters];
			for(int j=0;j<clusters;j++){
				double[] merged=ArrayUtils.addAll(rtable.get(stable.get(i).fk).fl.get(j),stable.get(i).fl.get(j));
				//data[j]=merged;
				//double[][] wrap = new double[dR+dS][1]; 
				//for (int w=0;w<dR+dS;w++)
					//wrap[w][0]=merged[w];
				//RealMatrix dPoint = MatrixUtils.createRealMatrix(wrap);
				double meanz[] = new double[dR+dS];
				Arrays.fill(meanz, 0);
				MultivariateNormalDistribution No = new MultivariateNormalDistribution(meanz,isigma.get(j).getData());
				double result=No.density(merged);
				//RealMatrix tDPoint = dPoint.transpose();
				//RealMatrix p = (tDPoint.multiply(isigma.get(j))).multiply(dPoint);
				res.f[j]=result;
				sum=sum+pik[j]*result;
			}
			for(int j=0;j<clusters;j++){
				try{
				res.f[j]=(pik[j]*res.f[j])/sum;
				Nk[j]=Nk[j]+res.f[j];
				} catch(Exception e){
					e.printStackTrace();
				}
			}
			intmTable.put(i, res);
			res=null;
		}	
		for(int j=0;j<clusters;j++){
			pik[j]=Nk[j]/NS;
			if (pik[j]==1.0)
				check=1;
		}
	}
	
	static void MstepMu(double []Nk,HashMap<Integer,GammaN> intmTable,HashMap<Integer,CoordinatesR> rtable,double[][] mu,HashMap<Integer,CoordinatesS> stable,ArrayList<RealMatrix> isigma,double[] pik){
		double[][] sum = new double[clusters][dR+dS];
		for (int i=1;i<=stable.size();i++){
			for(int j=0;j<clusters;j++){
				int in=0;
				for(int k=0;k<dR+dS;k++){
					if(k<dR)
					  sum[j][k]=sum[j][k]+(intmTable.get(i).f[j])*(rtable.get(stable.get(i).fk).f.get(k));
				    else {
				      sum[j][k]=sum[j][k]+intmTable.get(i).f[j]*stable.get(i).f.get(in);
				      in=in+1;
				    }
				}
			}
		}
		for (int i=0;i<clusters;i++){
			for(int k=0;k<dR+dS;k++){
			mu[i][k]=sum[i][k]/Nk[i];
			}
		}
		//mean calcualted 
		
	}
	
	static void MstepSig(double []Nk,HashMap<Integer,GammaN> intmTable,HashMap<Integer,CoordinatesR> rtable,double[][] mu,HashMap<Integer,CoordinatesS> stable,ArrayList<RealMatrix> sigma,double[] pik ){
		sigma = new ArrayList<RealMatrix>();
		for (int i=0;i<clusters;i++){
			
			double [][] sum = new double[dR+dS][dR+dS];
			RealMatrix sumR = MatrixUtils.createRealMatrix(sum);
			for (int j=1;j<=stable.size();j++){
				double [][] dp = new double[dR+dS][1];
				int ind=0;
				for(int k=0;k<dR+dS;k++){
					if(k<dR)
				     dp[k][0] = rtable.get(stable.get(j).fk).fl.get(i)[k];
					else{
					 dp[k][0]=stable.get(j).fl.get(i)[ind];
					 ind=ind+1;
					}
				}
				RealMatrix dataPoint = MatrixUtils.createRealMatrix(dp);
				sumR=sumR.add((dataPoint.multiply(dataPoint.transpose())).scalarMultiply(intmTable.get(j).f[i]));
			}
			sigma.add(sumR.scalarMultiply(1/Nk[i]));
		}
	}
	
	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		HashMap<Integer,CoordinatesR> rtable = new HashMap<Integer,CoordinatesR>();
		HashMap<Integer,CoordinatesS> stable = new HashMap<Integer,CoordinatesS>();
		HashMap<Integer,GammaN> intmTable = new HashMap<Integer,GammaN>();
		try {
			Class.forName("org.postgresql.Driver");
		} catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
		String database = "Himanshu";
		String username="postgres";
		String password="strive";
		Statement s = null;
		Connection con = null;
		try {
			con = DriverManager.getConnection("jdbc:postgresql:" + database,
												username, password);
			s = con.createStatement();
		} catch(SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		int tries=0;
		//Choose number of iterations here.	
		double[][] mu = new double[clusters][dimr+dims];
		double [] pik = new double[clusters];
		ArrayList<RealMatrix> sigma = new ArrayList<RealMatrix>();
		Random rand = new Random();
		for (int i=0;i<clusters;i++){
			for (int j=0;j<(dimr+dims);j++)
              mu[i][j]=rand.nextDouble();
		}
	    //while(tries<100){
		
		
		ResultSet rs = null;
		//Queries to select values from R and S tables
		String rsql = "select rid,";
		for(int j=0;j<clusters;j++){	
		 for (int i=1;i<=dR;i++){
			 double var =mu[j][i-1];
			 String ttemp1="";
			 if(mu[j][i-1]<0){
				  var = mu[j][i-1]*-1.0;
			      ttemp1=ttemp1+"+"+var;
			 }else if(mu[j][i-1]>=0)
				 ttemp1=ttemp1+"-"+var;
		   rsql=rsql+"x"+i+ttemp1;  
	       if (j!= clusters-1 || i!=dR)
	         rsql = rsql+",";
	     }
		} 
		rsql = rsql+" from rghr order by rid";
		String ssql = "select sid,fk,";
		for(int j=0;j<clusters;j++){
		 for (int i=1;i<=dS;i++){
			 double var =mu[j][i-1+dR];
			 String ttemp1="";
			 if(mu[j][i-1+dR]<0){
				  var = mu[j][i-1+dR]*-1.0;
			      ttemp1=ttemp1+"+"+var;
			 }else if(mu[j][i-1+dR]>=0)
				 ttemp1=ttemp1+"-"+var;
		   ssql=ssql+"y"+i+ttemp1;  
	       if (j!= clusters-1 || i!=dS)
	         ssql = ssql+",";
	     }
		} 
		ssql = ssql+" from sghr order by sid";
		//String sql = "SELECT rid,x1,x2,x3 FROM rf";
		//String sql1 = "SELECT sid,fk,y1,y2,y3,y4,y5,y6,y7 FROM sf";
		 int mrid=0;
		    //Query execution to fill the maps rtable and stable. Note we store the RID/SID and the 
		    //entire coordinate object containing the various features and other attributes.
			try {
				rs = s.executeQuery(rsql);
				while(rs.next()) {
					CoordinatesR c= new CoordinatesR();
					c.fl = new ArrayList<double[]>();
					c.f = new ArrayList<Double>();
					double[] dimInClus = new double[dR];
					int clCount=0;
					int ck=1;
					for (int i=2;i<=((clusters*dimr)+1);i++){
						dimInClus[clCount]=Double.parseDouble(rs.getString(i));
						if(ck==1)
						c.f.add(dimInClus[clCount]+mu[0][clCount]);
						clCount=clCount+1;
						if(clCount == dR){
						ck=0;
					    c.fl.add(dimInClus);
					    //check of this makes it null;
					    dimInClus=null;
					    dimInClus = new double[dR];
					    clCount=0;
						}	
					}
					/*c.f = new ArrayList<Double>();
					for (int i=2;i<=dimr+1;i++){
					    c.f.add(Double.parseDouble(rs.getString(i)));
					}*/
					rtable.put(Integer.parseInt(rs.getString(1)),c);
					if(mrid<Integer.parseInt(rs.getString(1)))
						mrid=Integer.parseInt(rs.getString(1));
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
			int msid=0;
			try {
				rs = s.executeQuery(ssql);
				while(rs.next()) {
					CoordinatesS c= new CoordinatesS();
					//
					c.fl = new ArrayList<double[]>();
					c.f = new ArrayList<Double>();
					double[] dimInClus = new double[dS];
					int clCount=0;
					int ck=1;
					for (int i=3;i<=((clusters*dims)+2);i++){
						dimInClus[clCount]=Double.parseDouble(rs.getString(i));
						if(ck==1)
							c.f.add(dimInClus[clCount]+mu[0][clCount+dR]);
						clCount=clCount+1;
						if(clCount == dS){
					    ck=0;
					    c.fl.add(dimInClus);
					    //check of this makes it null;
					    dimInClus=null;
					    dimInClus = new double[dS];
					    clCount=0;
						}	
					}
					//
					/*c.f = new ArrayList<Double>();
					for (int i=3;i<=dims+2;i++)
						c.f.add(Double.parseDouble(rs.getString(i)));	*/
					c.fk = Integer.parseInt(rs.getString(2));
					stable.put(Integer.parseInt(rs.getString(1)),c);
					if(msid<Integer.parseInt(rs.getString(1)))
						msid=Integer.parseInt(rs.getString(1));
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
          //read the data
			/*for (int i=1;i<=mrid;i++)
			 prepareData(mu,rtable.get(i));
			for (int i=1;i<msid;i++)
				prepareDataS(mu,stable.get(i));*/
			//System.out.println(tries);
			while(tries<3){
				if(tries == 0)
				initSigma(rtable,stable,sigma,pik);
				// generated mean, sigma and prepared data.
				/*for (int i=0;i<sigma.size();i++)
					isigma.add(MatrixUtils.inverse(sigma.get(i)));	*/
				double[] Nk = new double[clusters];
				//E-step
				int check=0;
				Estep(check,Nk,intmTable,rtable,mu,stable,sigma,pik );
				if (check == 1){
				    System.out.println("converged at iteration "+tries);
					tries=100;
				}
				//M-step
	            MstepMu(Nk,intmTable,rtable,mu,stable,sigma,pik);
		//////////////////////////////////////////
				rs = null;
				//Queries to select values from R and S tables
				int hint=0;
				rsql = "select rid,";
				for(int j=0;j<clusters;j++){
				 for (int i=1;i<=dR;i++){
					 double var =mu[j][i-1];
					 String ttemp1="";
					 if(mu[j][i-1]<0){
						  var = mu[j][i-1]*-1.0;
					      ttemp1=ttemp1+"+"+var;
					 }else if(mu[j][i-1]>=0)
						 ttemp1=ttemp1+"-"+var;
				   rsql=rsql+"x"+i+ttemp1;   
					 if (Double.isNaN(mu[j][i-1]))
						  hint=1;
			       if (j!= clusters-1 || i!=dR)
			         rsql = rsql+",";
			     }
				} 
				rsql = rsql+" from rghr order by rid";
				if(hint ==1){
					System.out.println("broke at"+tries);
					break;
				}
				ssql = "select sid,fk,";
				for(int j=0;j<clusters;j++){
				 for (int i=1;i<=dS;i++){
					 double var =mu[j][i-1+dR];
					 String ttemp1="";
					 if(mu[j][i-1+dR]<0){
						  var = mu[j][i-1+dR]*-1.0;
					      ttemp1=ttemp1+"+"+var;
					 }else if(mu[j][i-1+dR]>=0)
						 ttemp1=ttemp1+"-"+var;
				   ssql=ssql+"y"+i+ttemp1;  
				  if (Double.isNaN(mu[j][i-1+dR]))
					  break;
			       if (j!= clusters-1 || i!=dS)
			         ssql = ssql+",";
			     }
				} 
				ssql = ssql+" from sghr order by sid";
				//String sql = "SELECT rid,x1,x2,x3 FROM rf";
				//String sql1 = "SELECT sid,fk,y1,y2,y3,y4,y5,y6,y7 FROM sf";
				 mrid=0;
				    //Query execution to fill the maps rtable and stable. Note we store the RID/SID and the 
				    //entire coordinate object containing the various features and other attributes.
					try {
						//System.out.println(rsql);
						rs = s.executeQuery(rsql);
						while(rs.next()) {
							CoordinatesR c= new CoordinatesR();
							c.fl = new ArrayList<double[]>();
							c.f = new ArrayList<Double>();
							double[] dimInClus = new double[dR];
							int clCount=0;
							int ck=1;
							for (int i=2;i<=((clusters*dimr)+1);i++){
								dimInClus[clCount]=Double.parseDouble(rs.getString(i));
								if(ck==1)
								c.f.add(dimInClus[clCount]+mu[0][clCount]);
								clCount=clCount+1;
								if(clCount == dR){
								ck=0;
							    c.fl.add(dimInClus);
							    //check of this makes it null;
							    dimInClus=null;
							    dimInClus = new double[dR];
							    clCount=0;
								}	
							}
							/*c.f = new ArrayList<Double>();
							for (int i=2;i<=dimr+1;i++){
							    c.f.add(Double.parseDouble(rs.getString(i)));
							}*/
							rtable.put(Integer.parseInt(rs.getString(1)),c);
							if(mrid<Integer.parseInt(rs.getString(1)))
								mrid=Integer.parseInt(rs.getString(1));
						}
					} catch(SQLException e) {
						e.printStackTrace();
					}
					msid=0;
					try {
						rs = s.executeQuery(ssql);
						while(rs.next()) {
							CoordinatesS c= new CoordinatesS();
							//
							c.fl = new ArrayList<double[]>();
							c.f = new ArrayList<Double>();
							double[] dimInClus = new double[dS];
							int clCount=0;
							int ck=1;
							for (int i=3;i<=((clusters*dims)+2);i++){
								dimInClus[clCount]=Double.parseDouble(rs.getString(i));
								if(ck==1)
									c.f.add(dimInClus[clCount]+mu[0][clCount+dR]);
								clCount=clCount+1;
								if(clCount == dS){
							    ck=0;
							    c.fl.add(dimInClus);
							    //check of this makes it null;
							    dimInClus=null;
							    dimInClus = new double[dS];
							    clCount=0;
								}	
							}
							//
							/*c.f = new ArrayList<Double>();
							for (int i=3;i<=dims+2;i++)
								c.f.add(Double.parseDouble(rs.getString(i)));	*/
							c.fk = Integer.parseInt(rs.getString(2));
							stable.put(Integer.parseInt(rs.getString(1)),c);
							if(msid<Integer.parseInt(rs.getString(1)))
								msid=Integer.parseInt(rs.getString(1));
						}
					} catch(SQLException e) {
						e.printStackTrace();
					}
	   ///////////////////////////////////////////	
					MstepSig(Nk,intmTable,rtable,mu,stable,sigma,pik);

            System.out.println(tries);
	         tries=tries+1;		
  	 
	         
	}	
	    long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println(totalTime);
	}
	// common data processing methods
	static void initSigma(HashMap<Integer,CoordinatesR> rtable,HashMap<Integer,CoordinatesS> stable,ArrayList<RealMatrix> sigma,double[] pik){
		//Random rand = new Random();
		for (int i=0;i<clusters ;i++){
		    /*int items[] = new int[NS];
		    for (int ii=1;ii<=NS;ii++)
		    	items[ii-1]=ii;
			int j1 = items[rand.nextInt(items.length)];
			int j2= items[rand.nextInt(items.length)];
			int j3=  items[rand.nextInt(items.length)];
		    double[][] n = new double[3][dR];
			double[][] n1 = new double[3][dS];
			double[][] merged = new double[3][dR+dS];
			n1[0]= stable.get(j1).fl.get(i);	
		    n[0]=rtable.get(stable.get(j1).fk).fl.get(i);
		    merged[0]=ArrayUtils.addAll(n[0], n1[0]);
		    n1[1]= stable.get(j2).fl.get(i);	
		    n[1]=rtable.get(stable.get(j2).fk).fl.get(i);
		    merged[1]=ArrayUtils.addAll(n[1], n1[1]);
		    n1[2]= stable.get(j3).fl.get(i);	
		    n[2]=rtable.get(stable.get(j3).fk).fl.get(i);
		    merged[2]=ArrayUtils.addAll(n[2], n1[2]);
		    RealMatrix nv = MatrixUtils.createRealMatrix(merged);
		Variance v = new Variance();
        double var =v.evaluate(nv.getColumn(2));
        var=Math.pow(Math.abs(var), .5);
		Mean nm = new Mean();
	    double mu =nm.evaluate(nv.getColumn(3));*/
	    NormalDistribution obj= new NormalDistribution(0,1);
	    double [][] sam = new double[dR+dS][dR+dS];
	    for (int j=0;j<(dR+dS);j++)
	    sam[j] =obj.sample(dR+dS);
	    RealMatrix A = MatrixUtils.createRealMatrix(sam);
	    A=A.add(A.transpose()).scalarMultiply(.50);
		RealMatrix trial = MatrixUtils.createRealIdentityMatrix(dR+dS);
	    trial=trial.scalarMultiply(dR+dS);
	    A=A.add(trial);
	    sigma.add(A);
	    pik[i]=1.0/(clusters);
	   /* Mean tm = new Mean();
	     for(int jj=0;jj<dR+dS;jj++)
	         meanT[i][jj]=tm.evaluate(nv.getColumn(jj));*/
		  }
	}
	static void prepareData(double[][] mu,CoordinatesR c){
		c.fl = new ArrayList<double[]>();
		double[] temp = new double[dR];
			for(int j=0;j<clusters;j++){
				for (int i=0;i<c.f.size();i++){
			       temp[j]=c.f.get(i)-mu[j][i];
				}
				c.fl.add(temp);
				temp=null;
				temp = new double[dR];
			}	
	}
	
	static void prepareDataS(double[][] mu,CoordinatesS c){
		c.fl = new ArrayList<double[]>();
		double[] temp = new double[dS];
			for(int j=0;j<clusters;j++){
				for (int i=0;i<c.f.size();i++){
			       temp[j]=c.f.get(i)-mu[j][i+dR];
				}
				c.fl.add(temp);
				temp=null;
				temp = new double[dS];
			}	
	}
}
