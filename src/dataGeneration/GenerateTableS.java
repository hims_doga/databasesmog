/**
 * 
 */
package dataGeneration;

/**
 * @author Doga
 *
 */

/*
Program for K-Means Clustering using Postgres Database in Java
Author: Manav Sanghavi	Author Link: https://www.facebook.com/manav.sanghavi
www.pracspedia.com

To run the file:
javac -classpath postgresql.jar KMeansDatabase.java
java -classpath postgresql.jar KMeansDatabase
*/
import java.util.Random;

import org.apache.commons.math3.distribution.NormalDistribution;

import java.sql.*;
import java.sql.Statement;

class GenerateTableS {

public static void main(String args[]) {
	try {
		Class.forName("org.postgresql.Driver");
	} catch(ClassNotFoundException e) {
		e.printStackTrace();
	}
	int NR = 6;
	int NS=10;
	String database = "Himanshu";
	String username="postgres";
	String password="strive";
	
	Connection con = null;
	try {

		con = DriverManager.getConnection("jdbc:postgresql:" + database,
											username, password);
	} catch(SQLException e) {
		e.printStackTrace();
		System.exit(0);
	}
	//INSERT INTO public.r(rid, x) VALUES (?, ?);
	Statement stmt = null;
	NormalDistribution ds= new NormalDistribution(0,1);
	NormalDistribution ds1= new NormalDistribution(6,1);
	String sqld = "DELETE FROM sf";
	try {
		stmt = con.createStatement();
		con.setAutoCommit(false);
		stmt.executeUpdate(sqld);
        stmt.close();
		con.commit();
	} catch(SQLException e) {
		e.printStackTrace();
	}	
	Random rn = new Random();
	for (int i=1; i<=NS; i++){
		double[] a = new double[3];
		int fk=0;
	if(i%2 == 0){
		a=ds.sample(7);
		fk=2*(1 + rn.nextInt((NR/2)-1));
	}
	else{
		a=ds1.sample(7);
		fk=2*(1 + rn.nextInt((NR/2)-1))+1;
	}
	String sql="INSERT INTO mogs(sid,fk,y1,y2,y3) VALUES ("+i+","+fk+",";
	for (int j=0;j<3;j++){
		sql=sql+a[j];
		if(j!=2)
			sql=sql+",";
	}
	sql=sql+");";
		
	//String sql = "INSERT INTO sf(sid,fk,y1,y2,y3,y4,y5,y6,y7) VALUES ("+i+","+fk+","+j1+","+j2+","+j3+","+j4+","+j5+","+j6+","+j7+")";
	//String query = "SELECT RID, X FROM R";
	try {
		stmt = con.createStatement();
		con.setAutoCommit(false);
		stmt.executeUpdate(sql);
        stmt.close();
		con.commit();
	} catch(SQLException e) {
		e.printStackTrace();
	}	
	}
	System.out.println("Done");
}
}

